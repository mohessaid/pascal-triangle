#Pascal Traingle

##Introduction

I don't what the algorithm does in the first time I saw it! There was two student working on their thesis to get a Master degree in Mathematics. They developed this algorithm which generate some numbers according to a pattern with no name. I gave the name "Pascal Traingle" to it, because of the fame of that one.

##Options

I implement this program with two options: 

1. Print the outputs to the console.
2. Print the outputs to a file.

The first one will be effecient if the given depth is less than or equal to 20. If you want to generate the results of examples with a depth greater than 20, you should use a file to save them.

That's it! Enjoy!