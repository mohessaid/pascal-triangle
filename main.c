#include <stdio.h> // header files les libraries
#include <stdlib.h>

// Le debut de programme principale
int main()
{
    printf("\t\t**********************************************\n");
    printf("\t\t*          Universite de Jijel               *\n");
    printf("\t\t*            Promotion 2013                  *\n");
    printf("\t\t*                 MATH                       *\n");
    printf("\t\t**********************************************\n");
    int n; // La profondeur de triangle.
    char chemin[255]; // chemin pour enrigistrer le fichier resulat
    printf("\n\nDonnez le chemin de ficher (ex: d:\\nom_fichier.txt)\n\nChemin: ");
    scanf("%s", chemin);
    printf("Donnez la profondeur de triangle: ");
    scanf("%d", &n); // Lire la valeur saisier par l'utilisateur.
    double triangle[2][n + 1]; // Table de deux ligne pour l'�tat courant et pr�cedent.
    int i, j, k, l; // Variables utilis�s dans les boucles de program.
    int cptligne = 0, cptdiago = 2;
    int perm = 0, longeur = 3;
    FILE *fp;
    fp = fopen(chemin, "w+");
    if (fp != NULL)
    {
        if ( n == 0)
        {
             printf("1\n");
             fprintf(fp, "1\n");
        }
        else if ( n == 1)
        {
             printf("1\n1\t1\n");
             fprintf(fp, "1\n1\t1\n");
        }
        else if (n > 1)
        {
            cptligne = 1;
            printf("1\n1\t1\n");
            fprintf(fp, "1\n1\t1\n");
            triangle[0][0] = 1;
            triangle[0][1] = 4;
            triangle[0][2] = 1;
            printf("1\t4\t1\n");
            fprintf(fp, "1\t4\t1\n");
            
            for (i = 3; i < n; i++)
            {
                triangle[1][0] = 1;
                printf("%.0f\t", triangle[1][0]);
                fprintf(fp, "%.0f\t", triangle[1][0]);
                triangle[1][1] = triangle[0][1] * 2 + i;
                printf("%.0f", triangle[1][1]); 
                fprintf(fp, "%.0f", triangle[1][1]);
                k = 1;
                if (((longeur + 1) % 2) == 0)
                {         
                    for (j = 2; j < ((longeur + 1) / 2); j++ )
                    {
                        triangle[1][j] = triangle[0][j] * (j + 1) + triangle[0][j - 1] * (i - k); 
                        printf("\t%.0f", triangle[1][j]); 
                        fprintf(fp, "\t%.0f", triangle[1][j]);
                        k++;       
                    }
                    triangle[1][((longeur + 1) / 2)] = triangle[1][((longeur + 1) / 2) - 1];
                    printf("\t%.0f", triangle[1][((longeur + 1) / 2)]);
                    fprintf(fp, "\t%.0f", triangle[1][((longeur + 1) / 2)]);
                } 
                else
                {
                    for (j = 2; j < ((longeur + 1) / 2); j++ )
                    {
                        triangle[1][j] = triangle[0][j] * (j + 1) + triangle[0][j - 1] * (i - k); 
                        printf("\t%.0f", triangle[1][j]);  
                        fprintf(fp, "\t%.0f", triangle[1][j]);
                        k++;      
                    }
                    triangle[1][((longeur + 1) / 2)] = (triangle[0][((longeur + 1) / 2)] * 2) * (((longeur + 1) / 2) + 1);
                    printf("\t%.0f", triangle[1][((longeur + 1) / 2)]); 
                    fprintf(fp, "\t%.0f", triangle[1][((longeur + 1) / 2)]);       
                }
                for (j = 0; j < longeur; j++ )
                {
                    triangle[0][j] = triangle[1][j];
                } 
                longeur++;      
                printf("\n");
                fprintf(fp, "\n");            
            } 
        }
        else
        {
            printf("On peut pas accepter des nombre negatifs\n");    
        }
    }
    else
    {
        printf("ERREUR: creation de fichier echoue!\n");
    }
    fclose(fp);
    
  
  system("PAUSE");	
  return 0;
}
